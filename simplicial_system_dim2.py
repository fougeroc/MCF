from sage.misc.cachefunc import cached_method
from sage.structure.sequence import Sequence
from sage.symbolic.ring import SR
from sage.rings.rational_field import QQ
from sage.arith.misc import binomial
from sage.functions.transcendental import hurwitz_zeta
from sage.matrix.constructor import matrix
from sage.modules.free_module_element import vector
from sage.rings.complex_arb import ComplexBall, CBF
from sage.rings.real_arb import RealBall
from sage.graphs.digraph import DiGraph
from sage.rings.number_field.number_field import NumberField
from sage.rings.power_series_ring import PowerSeriesRing
from sage.rings.polynomial.polynomial_ring import polygen
from sage.rings.qqbar import number_field_elements_from_algebraics
from sage.misc.latex_standalone import TikzPicture


from simplicial import SimplicialSystem
from cycles_and_distances import cycles, matrix_all_distances


def h_zeta(s, x):
    if isinstance(s, ComplexBall) or isinstance(s, RealBall):
        return s.zeta(x+1)
    else:
        return hurwitz_zeta(s, x+1)


class SimplicialSystemDim2(SimplicialSystem):
    def __init__(self, G):
        r"""
        INPUT:

        - ``G`` - a directed graph whose edges are labelled with 0 and 1 and such that
                  from each vertex there is an outgoing edge 0 and an outgoing edge 1
        """
        self._G_init = G.copy()
        self.vertex_label = G.vertices(sort=True)
        self.alphabet = sorted(set(G.edge_labels()))
        self.dimension = len(self.alphabet)

        for v in G.vertices(sort=False):
            edges = G.outgoing_edges(v)
            if len(edges) != 2:
                raise ValueError('does not have 2 outgoing edges at v={}'.format(v))
            l0 = edges[0][2]
            l1 = edges[1][2]
            if (l0, l1) != (0, 1) and (l0, l1) != (1, 0):
                raise ValueError('invalid labelling at v={}: outgoing edges are {}'.format(v, edges))

        H = DiGraph(multiedges=True, loops=True)
        for u, v, lab in G.edges(sort=False):
            H.add_edge(self.vertex_label.index(u), self.vertex_label.index(v), self.alphabet.index(lab))
        self._G = H

    def __repr__(self):
        return 'SimplicialSystemDim2(DiGraph({}, loops=True, multiedges=True))'.format(self._G.edges(sort=False))

    def _check_farey_density(self, density):
        if not isinstance(density, (tuple, list)) or len(density) != self._G.num_verts():
            raise ValueError('a Farey density must be a list of length len(V)={}'.format(self._G.num_verts()))

    def _check_gauss_density(self, density):
        V0, V1 = self.reached()
        if (not isinstance(density, (tuple, list)) or
            len(density) != 2 or
            not isinstance(density[0], (tuple, list)) or
            not isinstance(density[1], (tuple, list)) or
            len(density[0]) != len(V0) or
            len(density[1]) != len(V1)):
            raise ValueError('a Gauss density must be a pair of lists of lengths len(V0)={} and len(V1)={}'.format(len(V0), len(V1)))

    def is_invariant_density(self, density):
        r"""
        Return whether ``density`` define invariant density for this
        2-dimensional simplicial system.

        INPUT:

        - ``density`` - a list of `n` rational fractions in one variable
        where `n` is the number of vertices of this simplicial system.

        EXAMPLES::

            sage: R = QQ['x'].fraction_field()
            sage: x = R.gen()

        The Farey map::

            sage: G = DiGraph([(0, 0, 0), (0, 0, 1)], loops=True, multiedges=True)
            sage: S = SimplicialSystemDim2(G)
            sage: S.is_invariant_density([ 1 / x / (1-x) ])
            True
            sage: S.is_invariant_density([ x ]) or S.is_invariant_density([ 1 / x ])
            False

        The flip-flop extension leading to Gauss map::

            sage: G = DiGraph([(0, 0, 0), (1, 0, 0), (0, 1, 1), (1, 1, 1)], loops=True, multiedges=True)
            sage: S = SimplicialSystemDim2(G)
            sage: S.is_invariant_density([1/x, 1/(1-x)])
            True
            sage: S.is_invariant_density([1/x/(1-x), 1/x/(1-x)])
            False

        A group extension::

            sage: G = DiGraph([(0, 0, 0), (1, 1, 0), (0, 1, 1), (1, 0, 1)], loops=True, multiedges=True)
            sage: S = SimplicialSystemDim2(G)
            sage: S.is_invariant_density([1/x/(1-x), 1/x/(1-x)])
            True

        1 goes up and 0 goes down::

            sage: S = simplicial_systems.up_down(3)

        A flip-flop extension of the Gauss graph with two neutral edges that form a cycle::

            sage: S, [f0,f1,f2,f3] = simplicial_systems.golden(with_farey_density=True)
            sage: f0
            phi/(x^2 + phi*x)
            sage: f0.factor()
            (phi) * x^-1 * (x + phi)^-1
            sage: f1.factor()
            (-1) * (x - phi - 1)^-1
            sage: f2.factor()
            (x + phi)^-1
            sage: f3.factor()
            (phi) * (x - phi - 1)^-1 * (x - 1)^-1
            sage: S.is_invariant_density([f0, f1, f2, f3])
            True

        An flip-flop extension of the up-down 2:

            sage: S = SimplicialSystems.up_down(2)
            sage: SS = S.flip_flop_extension(plus_edges=[(0,0,0),(1,1,1)], minus_edges=[(1,0,0)], neutral_edges=[(0,1,1)])
            sage: SS.is_invariant_density([1/(1+x), 1/(x-2)/(x-3), 1/x/(1+x), (x^2-4*x+5)/(1-x)/(2-x)/(3-x)])
            True

        """
        return self.invariant_density_map_reduce(density, lambda x, y: x == y, all)

    def invariant_density_equations(self, density):
        return self.invariant_density_map_reduce(density, lambda x,y: x-y, lambda x: x)

    def invariant_density_map_reduce(self, density, f_map, f_reduce):
        self._check_farey_density(density)

        R = Sequence(density).universe()
        x = R.gen()

        # inverse branches
        h = [x / (1 + x), 1 / (2 - x)]
        dh = [hi.derivative() for hi in h]

        l = []
        for v in self._G:
            f = sum(dh[label] * density[u](h[label]) for (u, _, label) in self._G.incoming_edges(v))
            l.append(f_map(f, density[v]))

        return f_reduce(l)

    def density_convert_gauss_to_farey(self, density):
        r"""
        EXAMPLES::

            sage: S, f, g = simplicial_systems.ras(True,True)
            sage: S.density_convert_farey_to_gauss(g) == f
            True
        """
        self._check_gauss_density(density)

        V0, V1 = self.reached()
        R = Sequence(density[0] + density[1]).universe()
        x = R.gen()

        farey_density = [R.zero()] * self._G.num_verts()

        for i, v in enumerate(V0):
            farey_density[v] += 1 / x**2 * density[0][i]((1 - x) / x)
        for i, v in enumerate(V1):
            farey_density[v] += 1 / (1 - x)**2 * density[1][i](x / (1 - x))

        return farey_density

    def density_convert_farey_to_gauss(self, density):
        r"""
        EXAMPLES::

            sage: S, f, g = simplicial_systems.ras(True,True)
            sage: S.density_convert_farey_to_gauss(f) == g
            True

            sage: S = simplicial_systems.farey()
            sage: S.density_convert_farey_to_gauss([1/x+1/(1-x)])
            [[1/(x + 1)], [1/(x + 1)]]

        """
        self._check_farey_density(density)

        R = Sequence(density).universe()
        y = R.gen()

        plus_edges = [(u, v, lab) for u, v, lab in self._G.edges(sort=False) if lab == 1]
        minus_edges = [(u, v, lab) for u, v, lab in self._G.edges(sort=False) if lab == 0]

        V0, V1 = self.reached()
        n = self._G.num_verts()
        S, cover_density = self.flip_flop_extension(plus_edges=plus_edges, minus_edges=minus_edges, farey_density=density)
        return [[1/(1+y)**2 * cover_density[v](1/(1+y)) for v in V0], [1/(1+y)**2 * cover_density[v+n](y/(1+y)) for v in V1]]

    def is_gauss_invariant_density(self, density):
        r"""
        Test if a set of density in Gauss charts is invariant.
        The graph has to be of Gauss type, i.e. all pointing edges to a vertex must have the same label.

        EXAMPLES::

            sage: R = QQ['x'].fraction_field()
            sage: x = R.gen()

            sage: S = SimplicialSystems.up_down(2)
            sage: S.is_gauss_invariant_density([1/(1+x)]*2)
            True

            sage: S = SimplicialSystems.up_down(2)
            sage: SS = S.flip_flop_extension(plus_edges=[(0,0,0),(1,1,1)], minus_edges=[(1,0,0)], neutral_edges=[(0,1,1)])
            sage: SS.is_gauss_invariant_density([(x + 1)^-1 * (x + 2)^-1,  (1/2) * (x + 3/2)^-1 * (x + 2)^-1, (x + 2)^-1,  (x + 1)^-1 * (x + 3/2)^-1 * (x + 2)^-1 * (x^2 + 3*x + 5/2)])
            True
        """
        return self.is_invariant_density(self.density_convert_gauss_to_farey(density))

    def flip_flop_extension(self, plus_edges=None, minus_edges=None, neutral_edges=None, farey_density=None):
        r"""
        Construct the flip-flop extension of this automaton.

        Each edge should be either be specified by a pair ``(start, label)`` or
        a triple ``(start, end, label)``.

        EXAMPLES::

            sage: S = SimplicialSystemDim2(DiGraph([(0, 0, 0), (0, 0, 1)], loops=True, multiedges=True))
            sage: S = S.flip_flop_extension(plus_edges=[(0, 0)], minus_edges=[(0, 1)])
            sage: R = QQ['x'].fraction_field()
            sage: x = R.gen()
            sage: S.is_invariant_density([1/x, 1/(1-x)])
            True

            sage: S = simplicial_systems.farey()
            sage: SS, ddensity = S.flip_flop_extension(plus_edges=[(0,0,0)],minus_edges=[(0,0,1)],farey_density=[1/x/(1-x)])
            sage: SSS, dddensity = SS.flip_flop_extension(plus_edges=[(0, 0, 1), (0, 1, 0)], minus_edges=[ (1, 0, 1), (1, 1, 0)], farey_density=density)
            sage: SSS.is_invariant_density(dddensity)
            True
            sage: dddensity
            [-1/(x - 2), 1/(x^2 + x), 1/(x^2 - 3*x + 2), 1/(x + 1)]
        """
        plus_edges = list(plus_edges) if plus_edges else []
        minus_edges = list(minus_edges) if minus_edges else []
        neutral_edges = list(neutral_edges) if neutral_edges else []

        def reconstruct(start, label):
            for edge in self._G.outgoing_edges(start):
                if edge[2] == label:
                    return edge
            raise ValueError('no edge with start={} and label={}'.format(start, label))

        def check(start, end, label):
            for edge in self._G.outgoing_edges(start):
                if edge == (start, end, label):
                    return edge
            raise ValueError('no edge with start={}, end={} and label={}'.format(start, end, label))

        def fix_and_check(edge_list):
            for i, e in enumerate(edge_list):
                if len(e) == 2:
                    edge_list[i] = reconstruct(*e)
                elif len(e) == 3:
                    edge_list[i] = check(*e)
                else:
                    raise ValueError('invalid data for an edge {}'.format(e))
        fix_and_check(plus_edges)
        fix_and_check(minus_edges)
        fix_and_check(neutral_edges)

        all_edges = plus_edges + minus_edges + neutral_edges
        if len(all_edges) != self._G.num_edges() or len(set(all_edges)) != len(all_edges):
            raise ValueError('invalid input')

        edge_type = {}
        for e in plus_edges:
            edge_type[e] = 1
        for e in minus_edges:
            edge_type[e] = -1
        for e in neutral_edges:
            edge_type[e] = 0

        n = self._G.num_verts()
        G = DiGraph(2*n, loops=True, multiedges=True)
        for e in self._G.edges(sort=False):
            start, end, label = e
            if edge_type[e] == 1:
                G.add_edge(start, end + n, label)
                G.add_edge(start + n, end + n, label)
            elif edge_type[e] == -1:
                G.add_edge(start, end, label)
                G.add_edge(start + n, end, label)
            else:
                G.add_edge(start, end, label)
                G.add_edge(start + n, end + n, label)

        S = SimplicialSystemDim2(G)

        if farey_density:
            if neutral_edges:
                raise NotImplementedError
            R = Sequence(farey_density).universe()
            y = R.gen()

            cover_density = [0]*2*n
            for u, v, lab in plus_edges:
                cover_density[v + n] += 1/(1+y)**2 * farey_density[u](y/(1+y)) if lab == 0 else 1/(2-y)**2 * farey_density[u](1/(2-y))
            for u, v, lab in minus_edges:
                cover_density[v] += 1/(1+y)**2 * farey_density[u](y/(1+y)) if lab == 0 else 1/(2-y)**2 * farey_density[u](1/(2-y))

            return S, cover_density

        else:
            return S

    def letter_action(self, l):
        r"""
        Return the action of the letter ``l`` on the states.

        EXAMPLES::

            sage: S = SimplicialSystemDim2(DiGraph([(0, 0, 0), (0, 0, 1)], loops=True, multiedges=True))
            sage: S.letter_action(0)
            [0]
            sage: S.letter_action(1)
            [0]

            sage: S = SimplicialSystems.up_down(2)
            sage: S.letter_action(0)
            [0, 0]
            sage: S.letter_action(1)
            [1, 1]
        """
        ans = [None] * len(self._G.vertices(sort=False))
        for (u, v, uvl) in self._G.edges(sort=False):
            if uvl == l:
                ans[u] = v
        return ans

    @cached_method
    def reached(self):
        r"""
        Return set of vertices that are reached by finite paths ending with 0 and 1 respectively.

        EXAMPLES::

            sage: G = DiGraph([(0, 0, 0), (1, 0, 0), (0, 1, 1), (1, 1, 1)], loops=True, multiedges=True)
            sage: S = SimplicialSystemDim2(G)
            sage: S.reached()
            ([0], [1])
        """
        A0 = self.letter_action(0)
        A1 = self.letter_action(1)
        V0 = set(A0)
        V1 = set(A1)

        while True:
            V0new = set(A0[v] for v in V1)
            V1new = set(A1[v] for v in V0)

            while True:
                aux0 = V0new.union(set(A0[v] for v in V0new))
                aux1 = V1new.union(set(A1[v] for v in V1new))

                if aux0 == V0new and aux1 == V1new:
                    break

                V0new = aux0
                V1new = aux1

            if V0new == V0 and V1new == V1:
                return sorted(V0), sorted(V1)

            V0 = V0new
            V1 = V1new

        return V0, V1

    @cached_method
    def cycles_lengths(self,l):
        res = [0]*len(self._G)
        for c in cycles(self.letter_action(l)):
            for v in c:
                res[v] = len(c)
        return res

    @cached_method
    def distances(self,l):
        return matrix_all_distances(self.letter_action(l))

    def f(self, i, j, w, v, l, x0, s):
        d = self.distances(l)[w][v]
        if d == -1:
            return 0

        a = self.cycles_lengths(l)[v]
        if a == 0:
            if w == v:
                return 0

            else:
                return (-1)**i * sum(binomial(j, t) *
                                     (-x0)**(j-t) *
                                     binomial(2*s+t+i-1, i) /
                                     (x0 + d)**(2*s+t+i)
                                     for t in range(j+1))

        else:
            if w == v:
                return (-1)**i * sum(binomial(j, t) *
                                     (-x0)**(j-t)/a**(2*s+t+i) *
                                     binomial(2*s+t+i-1, i) *
                                     h_zeta(2*s+t+i, x0/a)
                                     for t in range(j+1))

            if w != v:
                return (-1)**i * sum(binomial(j, t) *
                                     (-x0)**(j-t)/a**(2*s+t+i) *
                                     binomial(2*s+t+i-1, i) *
                                     h_zeta(2*s+t+i, (x0-a+d)/a)
                                     for t in range(j+1))

    def truncated_transfer_operator(self, N, field=SR, x0=1, s=1):
        r"""
        Return a trunctation of the transfer operator.

        The transfer operator is acting on holomorphic functions. We truncate its
        action by projecting it to a finite dimensional subspace and hence obtain
        a matrix.

        The output is a pair of matrices corresponding to taking preimages under
        edges 0 and 1 respectively.

        INPUT:

        - ``N`` - truncation of power series as approximation

        - ``field`` - coefficients used in the matrices

        - ``x0`` - base point used for series expansion in (x-x0)^k

        - ``s`` - parameter for the transfer operator

        EXAMPLES::

            sage: S = simplicial_systems.farey()
            sage: M0, M1 = S.truncated_transfer_operator(3)
            sage: M0
            [                           1/6*pi^2 - 1                     -1/6*pi^2 + zeta(3)        1/90*pi^4 + 1/6*pi^2 - 2*zeta(3)]
            [                         -2*zeta(3) + 2              -1/30*pi^4 + 2*zeta(3) + 1       1/15*pi^4 - 4*zeta(5) - 2*zeta(3)]
            [                          1/30*pi^4 - 3              -1/30*pi^4 + 6*zeta(5) - 3 2/189*pi^6 + 1/30*pi^4 - 12*zeta(5) - 1]
            sage: M1
            [                           1/6*pi^2 - 1                     -1/6*pi^2 + zeta(3)        1/90*pi^4 + 1/6*pi^2 - 2*zeta(3)]
            [                         -2*zeta(3) + 2              -1/30*pi^4 + 2*zeta(3) + 1       1/15*pi^4 - 4*zeta(5) - 2*zeta(3)]
            [                          1/30*pi^4 - 3              -1/30*pi^4 + 6*zeta(5) - 3 2/189*pi^6 + 1/30*pi^4 - 12*zeta(5) - 1]
        """
        x0 = field(x0)
        s = field(s)

        V0, V1 = self.reached()

        M0 = matrix(field, N*len(V0), N*len(V1), lambda i,j : self.f(i % N, j % N, V1[j // N], V0[i // N], 0, x0, s))
        M1 = matrix(field, N*len(V1), N*len(V0), lambda i,j : self.f(i % N, j % N, V0[j // N], V1[i // N], 1, x0, s))

        return M0, M1

    def truncated_spectrum(self, N, field=SR, x0=1, s=1, order=True):
        M0, M1 = self.truncated_transfer_operator(N, field, x0, s)
        if order:
            M = M0*M1 if M0.nrows() <= M0.ncols() else M1*M0
        else:
            M = M1*M0 if M0.nrows() <= M0.ncols() else M0*M1
        return sorted([ev.real().abs().sqrt() for ev in M.eigenvalues()], reverse=True)

    def truncated_density(self, N, field=CBF, s=1):
        r"""
        Return the almost invariant density obtained from the truncated transfer operator.

        EXAMPLE ::

            sage: R = QQ['x'].fraction_field()
            sage: x = R.gen()

        For one vertex the density is 1/x/(1-x)
            sage: S = simplicial_systems.farey()
            sage: g = [[1/(1+x)]]*2
            sage: S.is_gauss_invariant_density(g)
            True
            sage: density_taylor = S.truncated_density(15,RDF)
            sage: errors = compare_density(density_taylor, g)
            sage: all(err < 1e-5 for err in errors)
            True

        Two 2-permutation covers with the same density
            sage: G = DiGraph([(0, 1, 0), (1, 0, 0), (0, 1, 1), (1, 0, 1)], loops=True, multiedges=True)
            sage: S = SimplicialSystemDim2(G)
            sage: g = [[1/(1+x)]*2]*2
            sage: S.is_gauss_invariant_density(g)
            True
            sage: density_taylor= S.truncated_density(15,RDF)
            sage: errors = compare_density(density_taylor, g)
            sage: all(err < 1e-5 for err in errors)
            True

            sage: G = DiGraph([(0, 1, 0), (1, 0, 0), (0, 0, 1), (1, 1, 1)], loops=True, multiedges=True)
            sage: S = SimplicialSystemDim2(G)
            sage: S.is_invariant_density([1/x/(1-x),1/x/(1-x)])
            True

        We know invariant density for Farey map
            sage: S, g = simplicial_systems.up_down(2, with_gauss_density=True)
            sage: density_taylor = S.truncated_density(15, RDF)
            sage: errors = compare_density(density_taylor, g)
            sage: all(err < 1e-5 for err in errors)
            True

        A non trivial flip flop example
            sage: S, g = simplicial_systems.ras(with_gauss_density=True)
            sage: density_taylor = S.truncated_density(10, RDF)
            sage: errors = compare_density(density_taylor, g)
            sage: all(err < 1e-3 for err in errors)

            sage: S, g = simplicial_systems.golden(with_gauss_density=True)
            sage: density_taylor = S.truncated_density(10, RDF)
            sage: errors = compare_density(density_taylor, g)
            sage: all(err < 1e-3 for err in errors)
        """
        M0, M1 = self.truncated_transfer_operator(N, field, 1, s)

        # TODO: Compute an approximate kernel
        density_0_vect  = max((M0*M1).eigenvectors_right(), key=lambda x: x[0].real())[1][0].apply_map(lambda x: x.real())
        density_1_vect  = max((M1*M0).eigenvectors_right(), key=lambda x: x[0].real())[1][0].apply_map(lambda x: x.real())

        return self.density_vect_to_psr([density_0_vect, density_1_vect])

    def density_vect_to_psr(self, density_vect):
        V0, V1 = self.reached()

        N = len(density_vect[0]) // len(V0)
        field = density_vect[0][0].parent()

        PSR = PowerSeriesRing(field, 'h', default_prec=N)
        h = PSR.gen()

        return [[sum([density_vect[0][i + j*N] * PSR(h**i) for i in range(N)]) for j in range(len(V0))],
                [sum([density_vect[1][i + j*N] * PSR(h**i) for i in range(N)]) for j in range(len(V1))]]

    def density_fun_to_vect(self, density, N, field=CBF, order=True):
        PSR = PowerSeriesRing(field, 'h', default_prec=N)
        h = PSR.gen()
        V0, V1 = self.reached()

        ans = []
        for v in V0 if order else V1:
            f = density[v]
            P = PSR(f(1+h))
            ans += P.list() + [0]*(N-len(P.list()))

        return vector(ans)

    def cover_truncated_density(self, N, field=CBF, x0=QQ(1)/2, s=1, order=True):
        G = self._G
        G_cover = DiGraph(multiedges=True, loops=True)

        incoming_labels = {v : set(lab for _, _, lab in G.incoming_edges(v)) for v in G.vertex_iterator()}
        for u, v, lab in G.edge_iterator():
            for u_in_lab in incoming_labels[u]:
                G_cover.add_edge(((u, u_in_lab), (v, lab), lab))
        S_cover = SimplicialSystemDim2(G_cover)

        h = [S_cover.truncated_density(N, field, x0, s, order) for order in [True,False]]
        return tuple(sum(sum(h[order][S_cover._vertex_labels.index((v, lab))]
                             for lab in set(l for _, _, l in G.incoming_edges(v)))
                         for order in [True, False])
                     for v in G.vertex_iterator())

    def is_gauss_type(self, certificate=False):
        r"""
        Test if all pointing edges to vertices have the same label.

        EXAMPLES::

            sage: UD2 = SimplicialSystems.up_down(2)
            sage: UD2.is_gauss_type()
            True
            sage: UD2.is_gauss_type(certificate=True)
            (True, [[0], [1]])
            sage: UD3 = SimplicialSystems.up_down(3)
            sage: UD3.is_gauss_type(certificate=True)
            (False, 1)
        """
        V = [[], []]
        for v in self._G.vertex_iterator():
            s = set(lab for _, _, lab in self._G.incoming_edges(v))
            if len(s) > 1:
                return (False, v) if certificate else False
            V[s.pop()].append(v)
        return (True, V) if certificate else True


def full_test(constructor, *args, **kwds):
    r"""
    EXAMPLES::

        sage: full_test(simplicial_systems.up_down, 2)
        sage: full_test(simplicial_systems.farey)
        sage: full_test(simplicial_systems.golden)
        sage: full_test(simplicial_systems.ras)
    """
    S, farey_density, gauss_density = constructor(*args, with_farey_density=True, with_gauss_density=True)
    assert S.is_invariant_density(farey_density)
    assert S.is_gauss_invariant_density(gauss_density)
    assert S.density_convert_farey_to_gauss(farey_density) == gauss_density
    assert S.density_convert_gauss_to_farey(gauss_density) == farey_density


def exactify_degree(rat, degree, prec):
    exact_coeffs = []
    QQx = QQ['x']

    for coeff in list(rat.numerator()) + list(rat.denominator()):
        P = algdep(numerical_approx(coeff, prec), degree)
        P = QQx(P)
        exact_coeff = AA.polynomial_root(P,  RIF(coeff) + RIF(-2**(-prec//2), 2**(-prec//2)))
        exact_coeffs.append(exact_coeff)
    number_field, exact_coeffs, _ = number_field_elements_from_algebraics(exact_coeffs, minimal=True, embedded=True)
    R = number_field[rat.numerator().variable_name()]

    return R(exact_coeffs[:rat.numerator().degree()+1]) / R(exact_coeffs[rat.numerator().degree()+1:])

def exactify_number_field(rat, number_field, prec):
    r"""
    EXAMPLES::

        sage: S, g = simplicial_systems.golden(with_gauss_density=True)
        sage: [[T00,T02],[T11,T13]] = S.truncated_density(30,RDF)
        sage: F00 = (T00/T00[0]).pade(0,1)
        sage: F02 = (T02/T02[0]).pade(1,2)

        sage: x = polygen(QQ)
        sage: R = NumberField(x^2 - x - 1, 'a', embedding=1.6)
        sage: a = R.gen()
        sage: F00 = exactify_number_field(F00, R, 10)
        sage: F02 = exactify_number_field(F02, R, 10)
        sage: F11 = F02
        sage: F13 = F00

        sage: S.is_gauss_invariant_density([[F00,F02],[F11, F13]])

        sage: S = simplicial_systems.irrational()
        sage: [[T00,T02],[T11,T13]] = S.truncated_density(50,RDF)
        sage: F00 = (T00/T00[0]).pade(0,2)
        sage: F02 = (T02/T02[0]).pade(0,1)

        sage: x = polygen(QQ)
        sage: R = NumberField(x^2 - x - 1, 'a', embedding=1.6)
        sage: a = R.gen()
        sage: F00 = exactify_number_field(F00, R, 10)
        sage: F02 = exactify_number_field(F02, R, 10)
        sage: F11 = F02
        sage: F13 = F00

        sage: S.is_gauss_invariant_density([[F00,F02],[F11, F13]])
    """
    a, d = number_field.gen(), number_field.degree()
    gens_exact = [a**i for i in range(d)]
    gens_approx = [numerical_approx(g, prec) for g in gens_exact]

    exact_coeffs = []
    for coeff in list(rat.numerator()) + list(rat.denominator()):
        v = pari.lindep(gens_approx + [numerical_approx(coeff, prec)]).sage()
        exact_coeffs.append(-sum(v[i] * gens_exact[i] for i in range(d)) / v[-1])
    R = number_field[rat.numerator().variable_name()]

    return R(exact_coeffs[:rat.numerator().degree()+1]) / R(exact_coeffs[rat.numerator().degree()+1:])


def farey_chart(f, N, order=True):
    r"""
    Convert the Taylor expansion at 1 of a measure density in Gauss chart on [0,1]
    to its expansion at 1/2 in Farey chart on [1/2,1] if order or on [0,1/2] otherwise.

    INPUT:

    - ``f`` - polynom corresponding to the Taylor expansion of density function at 1
    - ``N`` - order of the output expansion

    EXAMPLE::

        sage: PR = PolynomialRing(QQ,'X')
        sage: X = PR.gen()
        sage: f = (1+1+X).inverse_series_trunc(5)
        sage: farey_chart(f,5,True) == (1/2+X).inverse_series_trunc(5)
        True
            sage: farey_chart(f,5,False) == (1-(1/2+X)).inverse_series_trunc(5)
            True
        """
    if f == 0:
        return 0

    def compose_trunc(f, g, N):
        if f.parent() != g.parent() or len(f.variables()) > 1:
            raise ValueError
        return sum([f[i]*g.power_trunc(i, N) for i in range(N)])

    h = f.variables()[0]
    x = QQ(1)/2 + h

    if order:
        g = (1-x)*x.inverse_series_trunc(N) - 1
        return (x**2).inverse_series_trunc(N).multiplication_trunc(compose_trunc(f,g,N),N)

    else:
        g = x*(1-x).inverse_series_trunc(N) - 1
        return ((1-x)**2).inverse_series_trunc(N).multiplication_trunc(compose_trunc(f,g,N),N)


def close_polynomial(P, d):
    PSR = PowerSeriesRing(P.base_ring(), 'h')

    Q = PSR(P).pade(d, d)
    Q_num, Q_den = Q.numerator(), Q.denominator()

    A = P.parent()

    def remove_small(P):
        return A([coeff for coeff in P if coeff.abs() > 1e-2])

    R_num, R_den = remove_small(Q_num), remove_small(Q_den)
    alpha = min(R_num[R_num.degree()], R_den[R_den.degree()])
    R_num, R_den = R_num/alpha, R_den/alpha

    return R_num, R_den

def compare_density(density_psr, density_fun):
    PSR = density_psr[0][0].parent()
    h = PSR.gen()

    if len(density_psr[0]) != len(density_fun[0]):
        raise ValueError

    errors = []
    for i in range(2):
        err = 0
        for j in range(len((density_psr[i]))):
            PS = PSR(density_psr[i][j])
            f  = density_fun[i][j](1+h)
            err += (f/f[0] - PS/PS[0]).polynomial().norm(2)
        errors.append(err)

    return errors

