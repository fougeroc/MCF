from sage.graphs.digraph import DiGraph
from copy import copy

from sage.plot.point import point
from sage.plot.graphics import Graphics
from sage.plot.colors import hue, rainbow
from sage.modules.free_module_element import vector

from sage.matrix.constructor import matrix
from sage.matrix.special import identity_matrix

from sage.misc.prandom import random
from math import sqrt, log
from sage.rings.real_mpfr import RR
from sage.rings.integer_ring import ZZ
from sage.rings.real_double import RDF
from sage.structure.sage_object import SageObject
from sage.misc.cachefunc import cached_method

import itertools
# import simplicial_system_dim2



def check_simplicial_system_graph(G):
    if any(lab is None for lab in G.edge_labels()):
        raise ValueError("All edges must be labeled")

    for v in G.vertices(sort=False):
        labels = [e[2] for e in G.outgoing_edges(v)]
        if any(labels.count(lab) > 1 for lab in labels):
            raise ValueError(
                "Two outgoing edges with the same labels going out of vertex " + str(v))


class SimplicialSystem(SageObject):
    def __init__(self, G, alphabet=None):
        r"""
        A simplicial system as in [Fou20].

        INPUT:

        * ``G`` - A labeled directed graph

        EXAMPLE::

            sage: from simplicial import *
            sage: G = DiGraph([(0,0,'0'),(0,0,'1')],loops=True)
            sage: SimplicialSystem(G)
            SimplicialSystem(DiGraph([(0, 0, '1')], loops=True, multiedges=True))

            sage: G = DiGraph([(0,0,0),(0,0,0)],loops=True, multiedges=True)
            sage: S = SimplicialSystem(G)
            Traceback (most recent call last):
            ...
            ValueError: Two outgoing edges with the same labels going out of vertex 0



        REFERENCES::
        .. [Fou20] Fougeron, "Dynamical properties of simplicial systems
           and continued fraction algorithms", 2020
        """
        check_simplicial_system_graph(G)

        self._G = copy(G)
        self.edges = G.edges()
        self.vertices = G.vertices()
        self.alphabet = copy(alphabet) if alphabet else list(set(G.edge_labels()))
        self.dimension = len(self.alphabet)

    def __copy__(self):
        G = copy(self._G)
        return SimplicialSystem(G)

    def __repr__(self):
        return 'SimplicialSystem(DiGraph({}, loops=True, multiedges=True))'.format(self.edges)

    def plot(self, **kwargs):
        return self._G.plot(edge_labels=True, color_by_label=True, **kwargs)

    def tikz(self, color_by_label=True):
        from sage.misc.latex_standalone import TikzPicture
        tikz = TikzPicture.from_graph(self._G, merge_multiedges=False, color_by_label=color_by_label, rankdir="right")
        tikz.add_usepackage('amstext')
        return tikz

    def check_twin_labeling(self, twin_labels):
        if isinstance(twin_labels, list):
            twin_labels = {e: twin_labels[i] for i, e in enumerate(self.edges)}

        for v in self.vertices:
            outgoing_labels = [twin_labels[e] for e in self._G.outgoing_edges(v)]
            if len(outgoing_labels) != len(set(outgoing_labels)):
                return False
        return True

    def twin_extension(self, twin_labels):
        if isinstance(twin_labels, list):
            twin_labels = {e: twin_labels[i] for i, e in enumerate(self.edges)}

        S = SimplicialSystemTwin(self._G)
        S._twin_labels = twin_labels
        return S

    def invertible_extension(self, reverse_labels):
        r"""
        Return an invertible extension of the simplicial system by defining a simplicial systems on the reverted graph.
        See documentation of SimplicialSystemExtension for details.
        """
        if isinstance(reverse_labels, list):
            reverse_labels = {e: reverse_labels[i] for i, e in enumerate(self.edges)}

        ingoing_labels = [[] for _ in range(self._G.num_verts())]
        for e in self.edges:
            if reverse_labels[e] in ingoing_labels[e[1]]:
                raise ValueError(
                "Two ingoing edges with the same labels going out of vertex " + str(e[1]))
            else:
                ingoing_labels[e[1]].append(reverse_labels[e])

        S = SimplicialSystemExtension(self._G)
        S._reverse_labels = reverse_labels

        return S

    def check_labeling(self, edge_labels):
        ingoing_labels = [[] for _ in range(self._G.num_verts())]
        for i, (u, v, _) in enumerate(self.edges):
            if edge_labels[i] in ingoing_labels[v]:
                return False
            else:
                ingoing_labels[v].append(edge_labels[i])
        return True

    def dual_iterator(self, n=None):
        G = self._G
        vertices = self.vertices
        edges = self.edges
        min_n = max(len(G.incoming_edges(v)) for v in vertices)
        if n is None:
            n = min_n
        if n < min_n:
            raise ValueError

        def first_labels(iterable, limit=5):
            seen = set()
            for element in iterable:
                if element not in seen:
                    seen.add(element)
                    yield element
                if len(seen) == limit:
                    break

        def label_iterator(n, k):
            for ordered_labels in itertools.combinations(range(n), k):
                for labels in itertools.permutations(ordered_labels):
                    yield labels

        for L in itertools.product(*[label_iterator(n, len(G.incoming_edges(vertex))) for vertex in vertices]):
            labels = [-1] * G.num_edges()
            in_edges_count = [0] * G.num_verts()
            for i, (u, v, _) in enumerate(edges):
                labels[i] = L[v][in_edges_count[v]]
                in_edges_count[v] += 1
            if len(set(labels)) != n or list(first_labels(labels)) != list(range(n)):
                continue
            if self.check_labeling(labels) and self.dual(labels).pairing_equations().dimension() > 0:
                yield self.dual(labels)

    def twin_iterator(self, n=None):
        G = self._G
        vertices = self.vertices
        edges = self.edges
        min_n = max(len(G.incoming_edges(v)) for v in vertices)
        if n is None:
            n = min_n
        if n < min_n:
            raise ValueError

        def first_labels(iterable, limit=5):
            seen = set()
            for element in iterable:
                if element not in seen:
                    seen.add(element)
                    yield element
                if len(seen) == limit:
                    break

        def label_iterator(n, k):
            for ordered_labels in itertools.combinations(range(n), k):
                for labels in itertools.permutations(ordered_labels):
                    yield labels

        for L in itertools.product(*[label_iterator(n, len(G.outgoing_edges(vertex))) for vertex in vertices]):
            labels = [-1] * G.num_edges()
            out_edges_count = [0] * G.num_verts()
            for i, (u, v, _) in enumerate(edges):
                labels[i] = L[u][out_edges_count[u]]
                out_edges_count[u] += 1
            if len(set(labels)) != n:
                continue
            if self.check_twin_labeling(labels):
                if self.twin_extension(labels).pairing_equations().dimension() > 0:
                    yield self.twin_extension(labels)
                else:
                    print(labels)


    def reduce(self):
        r"""
        Remove the vertices with one outgoing edge and return a reduced simplicial system with "same" dynamics.

        EXAMPLE::

            sage: from simplicial import *
            sage: G = DiGraph([(0,0,0),(0,1,1),(1,2,0),(2,0,1)],loops=True, multiedges=True)
            sage: S = SimplicialSystem(G)
            sage: S.reduce()
            SimplicialSystem(DiGraph([(0, 0, 0), (0, 0, 1)], loops=True, multiedges=True))

            sage: G = DiGraph([(0,0,0),(0,1,1),(1,2,'bla'),(2,0,3)],loops=True, multiedges=True)
            sage: S = SimplicialSystem(G)
            sage: S.reduce()
            SimplicialSystem(DiGraph([(0, 0, 0), (0, 0, 1)], loops=True, multiedges=True))

            sage: G = DiGraph([(0,1,0),(1,1,1)],loops=True, multiedges=True)
            sage: S = SimplicialSystem(G)
            sage: S.reduce()
            SimplicialSystem(DiGraph([], loops=True, multiedges=True))

        """
        G = self._G
        removed_vertices, points_to = [], []
        for u in self.vertices:
            outgoing_edges = G.outgoing_edges(u)
            if len(outgoing_edges) == 1:
                _, v, _ = outgoing_edges[0]
                removed_vertices.append(u)
                points_to.append(v)

        for i, u in enumerate(removed_vertices):
            end_vertex = points_to[i]
            visited_vertex = [u]
            while end_vertex in removed_vertices and end_vertex != u:
                end_vertex = points_to[removed_vertices.index(end_vertex)]
                if end_vertex in visited_vertex:
                    end_vertex = u
                visited_vertex.append(end_vertex)
            points_to[i] = end_vertex

        H = DiGraph(loops=True, multiedges=True)
        for u, v, lab in G.edges(sort=False):
            if u not in removed_vertices:
                if v in removed_vertices:
                    H.add_edge((u, points_to[removed_vertices.index(v)], lab))
                else:
                    H.add_edge((u, v, lab))

        # if len(set(H.edge_labels())) == 2:
        #     return simplicial_system_dim2.SimplicialSystemDim2(H)
        return SimplicialSystem(H)

    def canonical_extension(self):
        G = self._G
        inverse_labels = {}
        for v in self.vertices:
            edges_incident = G.edges_incident(v)

            incident_labels = [e[2] for e in edges_incident]
            if len(incident_labels) < len(set(incident_labels)):
                raise ValueError("The canonical extension labeling does not define a simplicial system on its inverse !")

            for i, e in enumerate(edges_incident):
                inverse_labels[e] = incident_labels[i-1]



        return self.invertible_extension(dict(inverse_labels))

    def init_rand(self, vertex, distorsion_vector=None):
        return self.vertex(vertex).init_rand(distorsion_vector)

    def init_deterministic(self, vertex, length_vector=None):
        return self.vertex(vertex).init_deterministic(distorsion_vector)

    def vertex(self, vertex):
        if vertex not in self._G.vertices():
            raise ValueError("The vertex defining position: %s, should be in the graph !" % str(vertex))

        return SimplicialSystemVertex(self, vertex)

    def quotient_automorphisms(self):
        r"""
        Quotient the current simplicial system with its automorphism group.

        """
        automorphism_group = self._G.automorphism_group(edge_labels=True)
        vertices_orbit = [0]*self._G.num_verts()
        for i, orbit in enumerate(automorphism_group.orbits()):
            for v in orbit:
                vertices_orbit[v] = i

        H = DiGraph(loops=true, multiedges=true)
        for u, v, lab in H.edges(sort=false):
            H.add_edge((vertices_orbit[u], vertices_orbit[v], lab))

        return SimplicialSystem(H)

    def dynamical_core(self):
        r"""
        Compute the dynamical core of the graph.

        EXAMPLES::

            sage: from simplicial import *
            sage: S = simplicial_systems.Selmer(3)
            sage: S.minimal_factor()
            SimplicialSystem(DiGraph([(0, 1, 2), (0, 2, 0), (1, 0, 1), (1, 2, 0), (2, 0, 1), (2, 1, 2)], loops=True, multiedges=True))

        """
        from collections import defaultdict
        G = self._G
        vertices = tuple(G.vertices(sort=False))

        previous_groups = []
        vertices_groups = [vertices]
        while previous_groups != vertices_groups:
            previous_groups = vertices_groups

            new_groups = defaultdict(list)
            group_set = [] # set of groups of vertices which labeled edges point to the same group in vertices_groups
            for u in vertices:
                edge_ending_group = []
                for _, v, lab in G.outgoing_edges(u):
                    # associate to the label the group of vertices the corresponding edge points to
                    edge_ending_group.append((lab, next(group for group in vertices_groups if v in group)))
                # group vertices depending on their edge ending group
                new_groups[frozenset(edge_ending_group)].append(u)

            vertices_groups = [tuple(group) for group in new_groups.values()]

        H = DiGraph(loops=True, multiedges=True)

        for i, group in enumerate(vertices_groups):
            u = group[0]
            for _, v, lab in G.outgoing_edges(u):
                v_group = next(group for group in vertices_groups if v in group)
                H.add_edge((i, vertices_groups.index(v_group), lab))

        return SimplicialSystem(H)

    def n_cover(self, n, domain_vertices=None):
        if not domain_vertices:
            domain_vertices = self.current_vertices
            domain_graph = DiGraph(multiedges=True, loops=True)

        graph = self._G
        for v in domain_vertices:
            tree = [[e] for e in graph.outgoing_edges(v)]
            while not all([path[-1][1] in domain_vertices for path in tree]):
                tree = [path for path in tree if path[-1][1] in domain_vertices] +\
                       [path + [e] for path in tree if path[-1][1] not in domain_vertices for e in graph.outgoing_edges(path[-1][1])]

            domain_graph.add_edges([(v, path[-1][1], tuple(path)) for path in tree])

        domain_path = []
        for v in domain_graph.vertices():
            aux = [[e] for e in domain_graph.outgoing_edges(v)]
            for _ in range(n-1):
                aux = [path + [e] for path in aux for e in domain_graph.outgoing_edges(path[-1][1])]

            domain_path += aux

        n_graph = DiGraph(loops=True, multiedges=True)
        for path in domain_path:
            init = [e[2] for e in path]

            v = path[-1][1]
            n_graph.add_edges([(tuple(init), tuple(init[1:] + [(e,)]), e[2]) for e in graph.outgoing_edges(v) if e[1] in domain_vertices])
            n_graph.add_edges([(tuple(init), tuple(init + [(e,)]), e[2]) for e in graph.outgoing_edges(v) if e[1] not in domain_vertices])
            tree = [[e] for e in graph.outgoing_edges(v) if e[1] not in domain_vertices]

            while tree != []:
                for p in tree :
                    v = p[-1][1]
                    n_graph.add_edges([(tuple(init + [tuple(p)]), tuple(init[1:] + [tuple(p + [e,])]), e[2]) for e in graph.outgoing_edges(v) if e[1] in domain_vertices])
                    n_graph.add_edges([(tuple(init + [tuple(p)]), tuple(init + [tuple(p + [e,])]), e[2]) for e in graph.outgoing_edges(v) if e[1] not in domain_vertices])
                tree = [p + [e] for p in tree for e in graph.outgoing_edges(p[-1][1]) if e[1] not in domain_vertices]


        return SimplicialSystem(n_graph)




_simplex_proj = matrix([[-sqrt(3), sqrt(3), 0], [-1, -1, 2]], ring=RR)/2
def plot_simplex(frame=False):
    from sage.plot.polygon import polygon
    from sage.plot.line import line
    G = Graphics()
    G += polygon([[float(_simplex_proj[i, j]) for i in range(2)]
                 for j in range(3)], fill=False, axes=False)
    if frame:
        G += line([_simplex_proj*vector([1/2, 1/2, 0]), _simplex_proj*vector([0, 0, 1])], color='grey') +\
             line([_simplex_proj*vector([1/2, 0, 1/2]), _simplex_proj*vector([0, 1, 0])], color='grey') +\
             line([_simplex_proj*vector([0, 1/2, 1/2]), _simplex_proj*vector([1, 0, 0])], color='grey') +\
             line([_simplex_proj*vector([1/2, 0, 1/2]), _simplex_proj*vector([0, 1/2, 1/2])], color='grey') +\
             line([_simplex_proj*vector([1/2, 0, 1/2]), _simplex_proj*vector([1/2, 1/2, 0])], color='grey') +\
             line([_simplex_proj*vector([0, 1/2, 1/2]),
                   _simplex_proj*vector([1/2, 1/2, 0])], color='grey')

    return G

def plot_interval(frame=False):
    from sage.plot.line import line
    G = line([(0, 0), (1, 0)], axes=False, thickness=.1)
    return G


def top_lyapunov_exp(SS, n, p=None):
    r"""
    Return the top two Lyapunov exponents of a simplicial system.
    """
    dim = SS.dimension
    p = vector([random() for _ in range(dim)]) if not p else p
    p = p/sum(p)
    pp = copy(p)

    l = 0
    ll = 0
    lq = 0
    N = 1
    prev = SS.pos

    SSaux = copy(SS)
    k = 0
    for _ in range(n):
        s = sum(SSaux.q.values())
        if s > 1e4:
            lq += log(s)
            SSaux.q = {l: SSaux.q[l]/s for l in SSaux.q.keys()}
        if p.norm() > 1e4:
            l += log(p.norm())
            p = p/p.norm()
        if pp.norm() > 1e4:
            ll += log(pp.norm())
            pp = pp/pp.norm()

        lab = SSaux._step(label=True, inplace=False)
        R = SSaux._step_rauzy_matrix(lab, inplace=True)
        R = R.change_ring(RDF)
        p = p*R
        pp = pp*R.inverse().transpose()
        if SSaux.pos != prev:
            N += 1
            prev = SS.pos

    return (l/N, (ll-l)/N)



def _split(order):
    """
    Order is a string of n labels such that the domain corresponds to order[0] > order[1] > ... > order[n-2].
    The label order[n-1] is the missing label given for convenience.
    """


class SimplicialSystemVertex(SimplicialSystem):
    def __init__(self, S, vertex):
        self._G = S._G
        self.edges = S.edges
        self.vertices = S.vertices
        self.alphabet = S.alphabet
        self.dimension = S.dimension

        self.current_vertex = vertex

    def path_iterator(self, n):
        for g in self._G.all_paths_iterator(starting_vertices=[self.current_vertex], max_length=n, labels=True, report_edges=True):
            if len(g) == n:
                #s = [lab for _, _, lab in g]
                yield g

    def _sub_simplex(self, path, col=hue(.5), leg=None, fill=True, T=1):
        from sage.plot.polygon import polygon

        M = self.rauzy_matrix(path)
        return polygon([list(_simplex_proj*T*vector(M[:, j])/float(sum(T*vector(M[:, j])))) for j in range(3)], fill=fill, axes=False, rgbcolor=col, legend_label=leg)

    def decompose_simplex(self, n, leg=True, fill=True, T=1):
        l = list(self.path_iterator(n))
        G = plot_simplex()
        col = rainbow(len(l), 'rgbtuple')
        i = 0
        for g in l:
            G += self._sub_simplex(g, col=col[i],
                                   leg=[l for _,_,l in g] if leg else None, fill=fill, T=T)
            i += 1
        return G

    def outgoing_edges(self):
        r"""
        List labels of outgoing edges at current position.
        """
        return self._G.edges_incident(self.current_vertex)

    def outgoing_labels(self):
        r"""
        List labels of outgoing edges at current position.
        """
        return [lab for (_, _, lab) in self._G.edges_incident(self.current_vertex)]

    def _next_vertex(self, label):
        ans = next((v for (u, v, lab) in self._G.outgoing_edges(self.current_vertex) if lab == label), None)
        if ans is None:
            raise ValueError(
                "Label {} is not going out of vertex {}".format(label, self.current_vertex))
        return ans

    def path_reduce(self, path, init_value=None, f_map=None, f_end=None, labels_only=True, inplace=True):
        r"""
        Apply a map along a path in the graph.

        INPUT:

        * ``vertex`` - Starting vertex for the path
        * ``path`` - Sequence of labeled edges
        * ``init_value`` - Initial value for the computation
        * ``f_map`` - Applied map at each step. Takes four arguments, value at a step,
        list of winning labels and losing label.
        * ``f_end`` - Applied map at the end of the path. Takes two arguments, value
        and current vertex.
        """
        val = init_value
        count = 0
        init_vertex = self.current_vertex
        curr_vertex = init_vertex

        for edge in path:
            winners = self.outgoing_labels()
            if labels_only:
                loser = edge
                if loser not in winners:
                    raise ValueError("Invalid path. Label {} is not going out of vertex {}".format(loser, curr_vertex))
            else:
                curr_vertex, next_vertex, loser = edge
            winners.remove(loser)
            if f_map is not None:
                val = f_map(val, count, curr_vertex, winners, loser)
            curr_vertex = next_vertex if not labels_only else self._next_vertex(loser)
            self.current_vertex = curr_vertex
            count += 1
        if f_end is not None:
            val = f_end(val, curr_vertex)

        if not inplace:
            self.current_vertex = init_vertex

        return val

    def ending_vertex(self, path):
        """
        sage: S = simplicial_systems.Cassaigne()
        sage: S.ending_vertex(0, [1, 2, 0])
        0
        """
        self.path_reduce(path, inplace=False)
        return self

    def ending_distorsion(self, distorsion_vector, path):
        def new_distorsion(d_vect, count, vertex, winners, loser):
            v = d_vect.copy()
            v[self.alphabet.index(loser)] += sum(d_vect[self.alphabet.index(w)] for w in winners)
            return (v)
        return self.path_reduce(path, init_value=distorsion_vector, f_map=new_distorsion, inplace=False)

    def probability(self, distorsion_vector, path):
        return prod(distorsion_vector) / prod(self.ending_distorsion(distorsion_vector, path, inplace=False))

    def rauzy_matrix(self, path):
        r"""
        EXAMPLES::

            sage: G = DiGraph([(0, 1, 1), (1, 0, 0), (0, 0, 0), (0, 1, 1)], loops=True)
            sage: S = SimplicialSystem(G)
            sage: S.rauzy_matrix(0,1)
            [1 1]
            [0 1]

            sage: G = DiGraph([(0, 1, 1), (1, 2, 2), (2, 0, 0), (0, 0, 0), (1, 1, 1), (2, 2, 2)], loops=True)
            sage: S = SimplicialSystem(G)
            sage: S.rauzy_matrix(0,1)
            [1 1 0]
            [0 1 0]
            [0 0 1]

            sage: G = DiGraph([(0, 1, 'a'), (1, 2, 'b'), (2, 0, 'c'), (0, 0, 'c'), (1, 1, 'a'), (2, 2, 'b')], loops=True)
            sage: S = SimplicialSystem(G)
            sage: S.rauzy_matrix(0,'a')
            [1 0 0]
            [0 1 1]
            [0 0 1]
            sage: S.rauzy_matrix(0,'c')
            [1 0 0]
            [0 1 0]
            [0 1 1]
        """
        Id = identity_matrix(ZZ, self.dimension)

        def f_map(M, count, curr_vertex, winners, loser):
            A = copy(M)
            for w in winners:
                j_loser = self.alphabet.index(loser)
                j_winner = self.alphabet.index(w)
                A[:, j_loser] += A[:, j_winner]
            return A

        return self.path_reduce(path, f_map=f_map, init_value=Id, inplace=False)

    def comatrix(self, path):
        Id = identity_matrix(ZZ, self.dimension)

        def f_map(M, count, curr_vertex, winners, loser):
            A = copy(M)
            for w in winners:
                j_loser = self.alphabet.index(loser)
                j_winner = self.alphabet.index(w)
                A[:, j_winner] -= A[:, j_loser]
            return A

        return self.path_reduce(path, f_map=f_map, init_value=Id, inplace=False)


    def init_rand(self, distorsion_vector=None):
        return SimplicialSystemRandomPath(self, distorsion_vector)

    def init_deterministic(self, length_vector=None):
        if length_vector is None:
            length_vector = [random() for _ in self._S.alphabet]
        return SimplicialSystemDeterministicPath(self, length_vector)



class SimplicialSystemPath(SimplicialSystemVertex):
    def steps(self, n, with_loser=True, with_vertex=False, with_winners=False):
        def f_map(val, count, curr_vertex, winners, loser):
            output = []
            if with_vertex:
                output.append(curr_vertex)
            if with_winners:
                output.append(winners)
            if with_loser:
                output.append(loser)
            if len(output) == 1:
                output = output[0]
            return val + [output]
        res = self.path_reduce(n, init_value=[], f_map=f_map)
        if with_loser or with_vertex or with_winners:
            return res

    def rauzy_matrix(self, n, field=ZZ):
        r"""
        """
        Id = identity_matrix(field, self.dimension)

        def f_map(M, count, curr_vertex, winners, loser):
            A = copy(M)
            for w in winners:
                j_loser = self._S.alphabet.index(loser)
                j_winner = self._S.alphabet.index(w)
                A[:, j_loser] += A[:, j_winner]
            return A

        return self.path_reduce(n, f_map=f_map, init_value=Id)

    def plot_orbit(self, n, n_min=0, save='', frame=False, size=10):
        d = self.dimension
        if not d in [2, 3]:
            raise ValueError("Dimension should be equal to 3 in order to plot")

        num_verts = self._S._G.num_verts()
        if d == 2:
            plt = [plot_interval(frame) for _ in range(num_verts)]
            def f_map(plt, count, curr_vertex, winners, loser):
                plt[self.current_vertex] += point((self.length_vector[0] / sum(self.length_vector), 0), color=hue(1./(1+log(count+1))), size=size)
                return plt
        if d == 3:
            plt = [plot_simplex(frame) for _ in range(num_verts)]
            def f_map(plt, count, curr_vertex, winners, loser):
                plt[self.current_vertex] += point(_simplex_proj*vector(self.length_vector) / sum(self.length_vector), color=hue(1./(1+log(count+1))), size=size)
                return plt
        def f_save(plt, _):
            for i in range(num_verts):
                plt[i].save(save + '_' + str(self._S.vertices[i]) + '.png', axes=False)
        if not save:
            f_save=None

        return self.path_reduce(n, init_value=plt, f_map=f_map, f_end=f_save)

    def plot_cotangent(self, n, n_min=0, save='', fame=False, size=10):
        d = self.dimension
        if not d in [2, 3]:
            raise ValueError("Dimension should be equal to 3 in order to plot")

        num_verts = self._S._G.num_verts()
        if d == 2:
            plt = [plot_interval() for _ in range(2*num_verts)]
            def f_map(val, count, curr_vertex, winners, loser):
                plt, cotan_vect = val
                plt[self.current_vertex] += point((self.length_vector[0] / sum(self.length_vector), 0), color=hue(1./(1+log(count+1))), size=size)
                plt[self.current_vertex + num_verts] += point((cotan_vect[0] / sum(cotan_vect), 0), color=hue(1./(1+log(count+1))), size=size)
                for w in winners:
                    cotan_vect[w] += cotan_vect[loser]
                return plt, cotan_vect
        if d == 3:
            plt = [plot_simplex() for _ in range(2*num_verts)]
            def f_map(val, count, curr_vertex, winners, loser):
                plt, cotan_vect = val
                plt[self.current_vertex] += point(_simplex_proj*vector(self.length_vector) / sum(self.length_vector), color=hue(1./(1+log(count+1))), size=size)
                plt[self.current_vertex + num_verts] += point(_simplex_proj*vector(cotan_vect) / sum(cotan_vect), color=hue(1./(1+log(count+1))), size=size)
                for w in winners:
                    cotan_vect[w] += cotan_vect[loser]
                return plt, cotan_vect
        def f_save(val, _):
            plt, _ = val
            for i in range(num_verts):
                plt[i].save(save + '_' + str(self._S.vertices[i]) + '.png', axes=False)
                plt[i + num_verts].save(save + '_cotan_' + str(self._S.vertices[i]) + '.png', axes=False)
            return val
        if not save:
            f_save=None

        cotan_vect = [random() for _ in range(num_verts)]
        return self.path_reduce(n, init_value=(plt, cotan_vect), f_map=f_map, f_end=f_save)[0]



class SimplicialSystemRandomPath(SimplicialSystemPath):
    def __init__(self, V, distorsion_vector):
        self._G = V._G
        self.edges = V.edges
        self.vertices = V.vertices
        self.alphabet = V.alphabet
        self.dimension = V.dimension
        self.current_vertex = V.current_vertex

        if distorsion_vector is None:
            distorsion_vector = [1]*self.dimension
        elif isinstance(distorsion_vector, dict):
            distorsion_vector = [distorsion_vector[i] for i,v in enumerate(V._S.alphabet)]
        elif not (isinstance(distorsion_vector, list) and len(distorsion_vector) == len(V._S.alphabet)):
            raise ValueError

        self.distorsion_vector = distorsion_vector

    def path_reduce(self, n, init_value=None, f_map=None, f_end=None, inplace=False, domain_vertices=None):
        r"""
        Apply a map along a random path of length n.

        INPUT:

        * ``vertex`` - Starting vertex for the path
        * ``path`` - List of losing labels in the path
        * ``init_value`` - Initial value for the computation
        * ``f_map`` - Applied map at each step. Takes four arguments, value at a step,
        list of winning labels and losing label.
        * ``f_end`` - Applied map at the end of the path. Takes two arguments, value
        and current vertex.
        """
        from sage.misc.prandom import random

        val = init_value
        count = 0
        if not inplace:
            init_vertex = self.current_vertex
            init_dist = copy(self.distorsion_vector)

        while count < n and self.outgoing_labels():
            curr_vertex = self.current_vertex
            outgoing_edges = self.outgoing_edges()
            labels = [e[2] for e in outgoing_edges]
            # pick the loser randomly
            p = random()
            Q = [self.distorsion_vector[self.alphabet.index(lab)] for lab in labels]
            d = sum(Q)
            loser = next(lab for i, lab in enumerate(labels) if p < sum(Q[:i+1]) / d)
            # list winners
            labels.remove(loser)
            winners = labels
            edge = next(e for e in outgoing_edges if e[2] == loser)

            if f_map is not None:
                val = f_map(val, count, edge, winners)
            self.current_vertex = edge[1]
            self.distorsion_vector[self.alphabet.index(loser)] = sum(self.distorsion_vector[self.alphabet.index(w)] for w in winners)
            count += 1

        if f_end is not None:
            val = f_end(val, curr_vertex)

        if not inplace:
            self.current_vertex = init_vertex
            self.distorsion_vector = init_dist

        return val

    def labels(self, n):
        return self.path_reduce(n, init_value='', f_map=lambda val, _, loser, winners: val + loser[2])


class SimplicialSystemDeterministicPath(SimplicialSystemPath):
    def __init__(self, V, length_vector):
        self._G = V._G
        self.edges = V.edges
        self.vertices = V.vertices
        self.alphabet = V.alphabet
        self.dimension = V.dimension
        self.current_vertex = V.current_vertex

        if not isinstance(length_vector, dict):
            length_vector = {v: length_vector[i] for i,v in enumerate(S.alphabet)}
        self.length_vector = length_vector

    def path_reduce(self, n, init_value=None, f_map=None, f_end=None):
        r"""
        Apply a map along a random path of length n.

        INPUT:

        * ``n`` - Number of steps
        * ``init_value`` - Initial value for the computation
        * ``f_map`` - Applied map at each step. Takes four arguments, value at a step,
        vertex, list of winning labels and losing label.
        * ``f_end`` - Applied map at the end of the path. Takes two arguments, value
        and current vertex.
        """
        val = init_value
        count = 0
        while count < n and self._S.outgoing_labels(self.current_vertex):
            if min(self.length_vector.values()) < 1e-10:
                self.length_vector = [random() for _ in self.length_vector]
            curr_vertex = self.current_vertex
            labels = self._S.outgoing_labels(curr_vertex)
            loser = min(labels, key=lambda l: self.length_vector[l])
            labels.remove(loser)
            winners = labels
            if f_map is not None:
                val = f_map(val, count, curr_vertex, winners, loser)
            self.current_vertex = self._S._next_vertex(curr_vertex, loser)
            for w in winners:
                self.length_vector[w] -= self.length_vector[loser]
            count += 1
        if f_end is not None:
            val = f_end(val, curr_vertex)

        return val


def check_simplicial_system_extension_graph(G):
    if any(lab is None for lab in G.edge_labels()):
        raise ValueError("All edges must be labeled")

    for v in G.vertices(sort=False):
        outgoing_labels = [e[2][0] for e in G.outgoing_edges(v)]
        ingoing_labels = [e[2][1] for e in G.outgoing_edges(v)]

        if any(labels.count(lab) > 1 for lab in outgoing_labels):
            raise ValueError(
                "Two outgoing edges with the same labels going out of vertex " + str(v))

        if any(labels.count(lab) > 1 for lab in ingoing_labels):
            raise ValueError(
                "Two ingoing edges with the same labels going out of vertex " + str(v))

class SimplicialSystemTwin(SimplicialSystem):
    def __repr__(self):
        return 'Simplicial System of the graph DiGraph({}) with dual labels {}'.format(self.edges,self._reverse_labels)

    @cached_method
    def twin(self):
        H = DiGraph(multiedges=True, loops=True)
        new_reverse = {}
        for e in self.edges:
            r_lab = self._twin_labels[e]
            H.add_edge((e[0], e[1], r_lab))
            new_reverse[(e[0], e[1], r_lab)] = e[2]

        return SimplicialSystem(H).twin_extension(dict(new_reverse))

    def plot_twin(self):
        return self.twin().plot()

    def plot_with_twin(self):
        graphics_array([[self.plot(), self.plot_twin()]]).show(figsize=[10, 5])

    def twin_path(self, vertex, path):
        r"""
        Compute the twin path in the twin graph.
        """
        def f_map(val, count, curr_vertex, winners, loser):
            losing_edge = next(e for e in self._G.outgoing_edges(curr_vertex) if e[2] == loser)
            return val + [self._twin_labels[losing_edge]]
        return self.path_reduce(vertex, path, init_value=[], f_map=f_map)

    def pairing_equations(self, vertex=0):
        r"""
        EXAMPLES::
        """
        from _functools import reduce

        G = self._G
        visited = [False]*G.num_verts()
        visited[vertex] = True
        path = [[]]*G.num_verts()
        leaves = [vertex]
        loops = []
        condition = False
        while not condition:
            condition = all(visited)
            new_leaves = []
            for u in leaves:
                for _, v, lab in G.outgoing_edges(u):
                    if visited[v]:
                        loops.append((path[u] + [(u, v, lab)], path[v]))
                    else:
                        path[v] = path[u] + [(u, v, lab)]
                        visited[v] = True
                        new_leaves.append(v)
            leaves = new_leaves
        loops = [([lab for _, _, lab in p], [lab for _, _, lab in q]) for p, q in loops]

        def bilinear_equation(A,B):
            r"""
            Return matrix of the linear map M -> M' = A.transpose() * M * B.
            Where R * v_M = v_M' with (v_M)_(i+m*j) = M_(i,j).
            """
            m, n = B.nrows(), A.ncols()
            A_lift = matrix(m*n, m*n, sparse=True)
            B_lift = matrix(m*n, m*n, sparse=True)
            for i in range(m):
                k = i
                for j in range(n):
                    for l in range(n):
                        A_lift[i+m*j, k+m*l] = A[l,j]
            for i in range(m):
                for j in range(n):
                    l = j
                    for k in range(m):
                        B_lift[i+m*j, k+m*l] = B[k,i]
            assert A_lift*B_lift == B_lift*A_lift
            return B_lift*A_lift

        def path_equation(path):
            end_vertex = self.ending_vertex(vertex, path)
            A = self.rauzy_matrix(vertex, path)
            B = self.twin().rauzy_matrix(vertex, self.twin_path(vertex, path))
            return bilinear_equation(A, B)

        kernels = [(path_equation(p) - path_equation(q)).right_kernel() for p, q in loops]
        return reduce(lambda x, y: x.intersection(y), kernels)

class SimplicialSystemExtension(SimplicialSystem):
    def __repr__(self):
        return 'Simplicial System of the graph DiGraph({}) with dual labels {}'.format(self.edges,self._reverse_labels)

    @cached_method
    def inverse(self):
        H = DiGraph(multiedges=True, loops=True)
        new_reverse = {}
        for e in self.edges:
            r_lab = self._reverse_labels[e]
            H.add_edge((e[1], e[0], r_lab))
            new_reverse[(e[1], e[0], r_lab)] = e[2]

        return SimplicialSystem(H).invertible_extension(dict(new_reverse))

    def inverse_path(self, vertex, path):
        r"""
        Compute the inverse path in the inverse graph.
        """
        def f_map(val, count, curr_vertex, winners, loser):
            losing_edge = next(e for e in self._G.outgoing_edges(curr_vertex) if e[2] == loser)
            return [self._reverse_labels[losing_edge]] + val
        return self.path_reduce(vertex, path, init_value=[], f_map=f_map)


    def pairing_equations(self, vertex=0):
        r"""
        EXAMPLES::

            sage: G = DiGraph([(0,1,1),(2,1,1),(1,0,0),(2,0,0),(1,2,2),(0,2,2)])
            sage: S = SimplicialSystem(G)
            sage: SD = S.dual([0,0,1,1,2,2])
            sage: SD.pairing_equations(0)
            Free module of degree 9 and rank 1 over Integer Ring
            Echelon basis matrix:
            [1 1 1 1 0 1 1 1 0]
            sage: M = matrix(3, 3, SD.pairing_equations(0).basis()[0])
            sage: M
            [1 1 1]
            [1 0 1]
            [1 1 0]
            sage: SD.push_pairing(0, [1,0], M)
            [1 1 1]
            [1 0 1]
            [1 1 0]

            sage: S = simplicial_systems.Triangle(3)
            sage: SD = S.dual([0,0,1,1,2,2])
            sage: v = SD.pairing_equations(0).basis()[0]
            sage: M = matrix(3, 3, v)
            sage: M
            [1 1 1]
            [1 0 0]
            [1 1 0]
            sage: SD.push_pairing(0, [1,2,0], M)
            [1 1 1]
            [1 0 0]
            [1 1 0]

        """
        from _functools import reduce

        G = self._G
        visited = [False]*G.num_verts()
        visited[vertex] = True
        path = [[]]*G.num_verts()
        leaves = [vertex]
        loops = []
        condition = False
        while not condition:
            condition = all(visited)
            new_leaves = []
            for u in leaves:
                for _, v, lab in G.outgoing_edges(u):
                    if visited[v]:
                        loops.append((path[u] + [(u, v, lab)], path[v]))
                    else:
                        path[v] = path[u] + [(u, v, lab)]
                        visited[v] = True
                        new_leaves.append(v)
            leaves = new_leaves
        loops = [([lab for _, _, lab in p], [lab for _, _, lab in q]) for p, q in loops]

        def bilinear_equation(A,B):
            r"""
            Return matrix of the linear map M -> M' = A.transpose() * M * B.
            Where R * v_M = v_M' with (v_M)_(i+m*j) = M_(i,j).
            """
            m, n = B.nrows(), A.ncols()
            A_lift = matrix(m*n, m*n, sparse=True)
            B_lift = matrix(m*n, m*n, sparse=True)
            for i in range(m):
                k = i
                for j in range(n):
                    for l in range(n):
                        A_lift[i+m*j, k+m*l] = A[l,j]
            for i in range(m):
                for j in range(n):
                    l = j
                    for k in range(m):
                        B_lift[i+m*j, k+m*l] = B[k,i]
            assert A_lift*B_lift == B_lift*A_lift
            return B_lift*A_lift

        def path_equation(path):
            end_vertex = self.ending_vertex(vertex, path)
            A = self.rauzy_matrix(vertex, path)
            B = self.inverse().rauzy_matrix(end_vertex, self.inverse_path(vertex, path))
            return bilinear_equation(A, B.inverse())

        kernels = [(path_equation(p) - path_equation(q)).right_kernel() for p, q in loops]
        return reduce(lambda x, y: x.intersection(y), kernels)

    def push_pairing(self, vertex, path, pairing_matrix):
        end_vertex = self.ending_vertex(vertex, path)
        A = self.rauzy_matrix(vertex, path)
        B = self.inverse().rauzy_matrix(end_vertex, self.inverse_path(vertex, path))

        return A.transpose() * pairing_matrix * B.inverse()

    def invariant_density(self, Omega):
        n = Omega.nrows()
        R = PolynomialRing(QQ, names=['x' + str(i) for i in range(n)])
        F = R.fraction_field()
        v_in_Omega = vector(F.gens()) * Omega
        density = QQ(1)
        for v_i in v_in_Omega:
            density = density * F(v_i).inverse()
        return density.factor()

    def plot_inverse(self):
        return self.inverse().plot()

    def plot_with_inverse(self):
        graphics_array([[self.plot(), self.plot_inverse()]]).show(figsize=[10, 5])



class SimplicialSystemGenerators():
    @staticmethod
    def Cassaigne():
        G = DiGraph([(0, 1, 1), (2, 1, 1), (0, 2, 2),
                    (1, 2, 2), (2, 0, 0), (1, 0, 0)])
        return SimplicialSystem(G)

    @staticmethod
    def Rauzy():
        G = DiGraph([(0, 1, 1), (0, 2, 2), (1, 0, 0),
                    (2, 0, 0), (1, 1, 2), (2, 2, 1)], loops=True)
        return SimplicialSystem(G)

    @staticmethod
    def JacobiPerron():
        G = DiGraph({1: {11: '1', 21: '2', 31: '3'}, 11: {11: '1', 12: '2'}, 12: {12: '1', 1: '3'}, 21: {
                    21: '1', 22: '2'}, 22: {22: '1', 1: '3'}, 31: {31: '1', 32: '2'}, 32: {32: '1', 1: '3'}})
        return SimplicialSystem(G)

    @staticmethod
    def Brun(n):
        r'''
        Return win-lose graph semi-conjugated to Brun algorithm in dimension n.
        '''
        from sage.combinat.permutation import Permutations, Permutation

        G = DiGraph()
        P = Permutations(n)
        # image set vertices
        G.add_vertices([p.cycle_string() for p in P.iterator_range()])
        # intermediate vertices have a label with prefix the starting image set
        G.add_vertices([p.cycle_string() + str(i)
                       for i in range(n-1, 1, -1) for p in P.iterator_range()])
        for p in P.iterator_range():
            G.add_edge(p.cycle_string(), p.cycle_string() + str(n-1), p[n-1])
            c = Permutation(list(range(2, n+1)) + [1])
            G.add_edge(p.cycle_string(), (c*p).cycle_string(), p[1-1])
            for k in range(n-1, 2, -1):
                G.add_edge(p.cycle_string() + str(k),
                           p.cycle_string() + str(k-1), p[k-1])
                c = Permutation([k] + list(range(1, k)))
                G.add_edge(p.cycle_string() + str(k),
                           (c*p).cycle_string(), p[1-1])

            G.add_edge(p.cycle_string() + str(2), p.cycle_string(), p[2-1])
            c = Permutation([2, 1])
            G.add_edge(p.cycle_string() + str(2), (c*p).cycle_string(), p[1-1])

        return SimplicialSystem(G)

    @staticmethod
    def RauzyGasket(n):
        r'''
        Return win-lose graph defining Arnoux--Rauzy algorithm and Rauzy gasket in dimension n.
        '''
        from sage.combinat.permutation import Permutations, Permutation

        G = DiGraph()
        P = Permutations(n)
        G.add_vertices('h')
        G.add_vertices([p.cycle_string() for p in P.iterator_range()])
        G.add_vertices([p.cycle_string() + str(i) for i in range(n-1, 1, -1)
                       for p in P.iterator_range()])
        G.add_vertices([p.cycle_string() + '~' for p in P.iterator_range()])
        G.add_vertices([p.cycle_string() + '~' + str(k) + 'x' + str(i+1)
                       for p in P.iterator_range() for k in range(3, n) for i in range(k-2)])
        G.add_vertices([p.cycle_string() + '~' + str(n) + 'x' + str(i+1)
                       for p in P.iterator_range() for i in range(n-3)])

        for p in P.iterator_range():
            G.add_edge(p.cycle_string(), p.cycle_string() + str(n-1), p[n-1])
            c = Permutation(list(range(2, n+1)) + [1])
            G.add_edge(p.cycle_string(), (c*p).cycle_string() + '~', p[1-1])
            for k in range(n-1, 2, -1):
                G.add_edge(p.cycle_string() + str(k), p.cycle_string() + str(k-1), p[k-1])
                c = Permutation(list(range(2, k+1)) + [1])
                G.add_edge(p.cycle_string() + str(k), (c*p).cycle_string() + '~', p[1-1])

            G.add_edge(p.cycle_string() + str(2), p.cycle_string() + '~', p[2-1])
            c = Permutation([2, 1])
            G.add_edge(p.cycle_string() + str(2), (c*p).cycle_string() + '~', p[1-1])

            if n > 3:
                G.add_edge(p.cycle_string() + '~', p.cycle_string() + "~3x1", p[3-1])
            else:
                G.add_edge(p.cycle_string() + '~', p.cycle_string(), p[3-1])

            G.add_edge(p.cycle_string() + '~', 'h', p[1-1])
            for k in range(4, n):
                G.add_edge(p.cycle_string()+'~'+str(k-1)+'x'+str(k-1-2),
                           p.cycle_string()+'~'+str(k)+'x1', p[k-1])
                G.add_edge(p.cycle_string()+'~'+str(k-1)+'x'+str(k-1-2), 'h', p[1-1])
                for i in range(1, k-2):
                    G.add_edge(p.cycle_string()+'~'+str(k)+'x'+str(i),
                               p.cycle_string()+'~'+str(k)+'x'+str(i+1), p[k-1])
                    G.add_edge(p.cycle_string()+'~'+str(k)+'x'+str(i), 'h', p[1-1])

            if n > 3:
                G.add_edge(p.cycle_string()+'~'+str(n-1)+'x'+str(n-3),
                           p.cycle_string()+'~'+str(n)+'x1', p[n-1])
                G.add_edge(p.cycle_string()+'~'+str(n-1)+'x'+str(n-3), 'h', p[1-1])
                for i in range(1, n-2-1):
                    G.add_edge(p.cycle_string()+'~'+str(n)+'x'+str(i),
                               p.cycle_string()+'~'+str(n)+'x'+str(i+1), p[n-1])
                    G.add_edge(p.cycle_string()+'~'+str(n)+'x'+str(i), 'h', p[1-1])
                G.add_edge(p.cycle_string()+'~'+str(n)+'x'+str(n-3), p.cycle_string(), p[n-1])
                G.add_edge(p.cycle_string()+'~'+str(n)+'x'+str(n-3), 'h', p[1-1])

        return SimplicialSystem(G)

    @staticmethod
    def Triangle(n):
        G = DiGraph(loops=True)
        for i in range(n):
            G.add_edge((i,i,i))
            G.add_edge((i, (i+1) % n, (i+1) % n))
        return SimplicialSystem(G)

    @staticmethod
    def Selmer(n):
        r'''
        Return win-lose graph semi-conjugated to Selmer algorithm in dimension n.
        '''
        from sage.combinat.permutation import Permutations, Permutation
        G = DiGraph(loops=bool(n == 2))
        P = Permutations(n)
        G.add_vertices([str(p) for p in P.iterator_range()])
        for p in P.iterator_range():
            cn = Permutation(list(range(2, n+1)) + [1])
            c1 = Permutation(list(range(2, n)) + [1, n])
            G.add_edge(str(p), str(cn*p), p[-1])
            G.add_edge(str(p), str(c1*p), p[0])
        return SimplicialSystem(G)

    @staticmethod
    def RauzyDiagram(l):
        H = AbelianStratum(l)
        P = H.permutation_representative(reduced=False)
        G = DiGraph(P.rauzy_diagram().graph())
        for e in G.edge_iterator():
            if G.edge_label(e[0], e[1]) == 0:
                G.set_edge_label(e[0], e[1], e[0][1][-1])
            else:
                G.set_edge_label(e[0], e[1], e[0][0][-1])
        return SimplicialSystem(G)

    @staticmethod
    def QuadraticRauzyDiagram(l):
        Q = QuadraticStratum(l)
        P = Q.permutation_representative(reduced=False)
        G = DiGraph(P.rauzy_diagram().graph())
        Gp = copy(G)
        Gp.add_vertex('h')
        for v in G.vertex_iterator():
            l = list(G.outgoing_edge_iterator(v))
            if len(l) < 2:
                Gp.add_edge(l[0][0], 'h', 1-l[0][-1])
        for e in Gp.edge_iterator():
            if Gp.edge_label(e[0], e[1]) == 0:
                Gp.set_edge_label(e[0], e[1], e[0][1][-1])
            else:
                Gp.set_edge_label(e[0], e[1], e[0][0][-1])
        return SimplicialSystem(Gp)

    @staticmethod
    def RauzyDiagramToSS(l):
        H = AbelianStratum(l)
        P = H.permutation_representative(reduced=False)
        G = DiGraph(P.rauzy_diagram().graph())
        for e in G.edge_iterator():
            if G.edge_label(e[0], e[1]) == 0:
                G.set_edge_label(e[0], e[1], e[0][1][-1])
            else:
                G.set_edge_label(e[0], e[1], e[0][0][-1])
        return SimplicialSystem(G)

    @staticmethod
    def QuadraticRauzyDiagramToSS(l):
        Q = QuadraticStratum(l)
        P = Q.permutation_representative(reduced=False)
        G = DiGraph(P.rauzy_diagram().graph())
        Gp = copy(G)
        Gp.add_vertex('h')
        for v in G.vertex_iterator():
            l = list(G.outgoing_edge_iterator(v))
            if len(l) < 2:
                Gp.add_edge(l[0][0], 'h', 1-l[0][-1])
        for e in Gp.edge_iterator():
            if Gp.edge_label(e[0], e[1]) == 0:
                Gp.set_edge_label(e[0], e[1], e[0][1][-1])
            else:
                Gp.set_edge_label(e[0], e[1], e[0][0][-1])
        return SimplicialSystem(Gp)

    @staticmethod
    def ArnouxRauzyPoincare(q=None, pos=None):
        from sage.combinat.permutation import Permutations, Permutation

        perm = Permutations(3).list()
        G = DiGraph()

        tau_2 = Permutation('(1,2)').inverse()
        tau_3 = Permutation('(1,2,3)').inverse()

        for sigma in perm:
            G.add_edge(sigma, tau_3*sigma, str(sigma[0]))
            G.add_edge(sigma, str(sigma) + '_aux', str(sigma[-1]))
            G.add_edge(str(sigma) + '_aux', str(sigma) + '~', str(sigma[1]))
            G.add_edge(str(sigma) + '_aux', tau_2*sigma, str(sigma[0]))

            G.add_edge(str(sigma) + '~', sigma, str(sigma[-1]))
            G.add_edge(str(sigma) + '~', tau_3*sigma, str(sigma[0]))

        return SimplicialSystem(G, q, pos)

    @staticmethod
    def up_down(k, with_farey_density=False, with_gauss_density=False):
        G = DiGraph(k, loops=True)
        G.add_edges((i, min(i+1, k-1), 1) for i in range(k))
        G.add_edges((i, max(i-1, 0), 0) for i in range(k))
        if not with_farey_density and not with_gauss_density:
            return simplicial_system_dim2.SimplicialSystemDim2(G)

        output = [simplicial_system_dim2.SimplicialSystemDim2(G)]

        if with_farey_density:
            if k == 2:
                x = polygen(QQ, 'x')
                output.append([1/x, 1/(1-x)])
            else:
                raise NotImplementedError

        if with_gauss_density:
            if k == 2:
                x = polygen(QQ, 'x')
                output.append([[1/(1+x)], [1/(1+x)]])
            else:
                raise NotImplementedError

        return output

    @staticmethod
    def farey(with_farey_density=False, with_gauss_density=False):
        G = DiGraph([(0, 0, 0), (0, 0, 1)], loops=True, multiedges=True)
        if not with_farey_density and not with_gauss_density:
            return simplicial_system_dim2.SimplicialSystemDim2(G)

        output = [simplicial_system_dim2.SimplicialSystemDim2(G)]

        if with_farey_density:
            x = polygen(QQ, 'x')
            output.append([1/x/(1-x)])

        if with_gauss_density:
            x = polygen(QQ, 'x')
            output.append([[1/(1+x)]]*2)

        return output

    @staticmethod
    def golden(with_farey_density=False, with_gauss_density=False):
        G = DiGraph([(0,0,0), (0,1,1), (1,0,0), (1,3,1), (2,0,0), (2,3,1), (3,2,0), (3,3,1)], loops=True, multiedges=True)
        if not with_farey_density and not with_gauss_density:
            return simplicial_system_dim2.SimplicialSystemDim2(G)

        output = [simplicial_system_dim2.SimplicialSystemDim2(G)]

        if with_farey_density:
            x = polygen(QQ, 'x')
            K = NumberField(x**2 - x - 1, 'phi', embedding=(1+AA(5).sqrt())/2)
            phi = K.gen()
            phibar = 1/phi
            x = K['x'].fraction_field().gen()
            f0 = 1 / x / (phibar*x + 1)
            f1 = 1 / (phi**2 - x)
            f2 = 1 / (x + phi)
            f3 = 1 / (x - 1) / (phibar * x - phi)

            output.append([f0, f1, f2, f3])

        if with_gauss_density:
            x = polygen(QQ, 'x')
            K = NumberField(x**2 - x - 1, 'phi', embedding=(1+AA(5).sqrt())/2)
            phi = K.gen()
            phibar = 1/phi
            x = K['x'].fraction_field().gen()
            output.append([[(x + phi)**-1, (phi - 1) * (x + 1)**-1 * (x + phi)**-1],
                           [(phi - 1) * (x + 1)**-1 * (x + phi)**-1, (x + phi)**-1]])

        return output

    @staticmethod
    def ras(with_farey_density=False, with_gauss_density=False):
        r"""
        EXAMPLES::

            sage: S, f, g = simplicial_systems.ras(True,True)
            sage: 1/(1+x)^2*(g[1][0](1/(1+x)) + g[1][1](1/(1+x))) == g[0][0]
            True
            sage: g[1][0] == 1/(1+x)^2 * g[0][0](1/(1+x))
            True
        """
        S = SimplicialSystemGenerators.up_down(2)
        S = S.flip_flop_extension(plus_edges=[(0,0,0),(1,1,1)], minus_edges=[(1,0,0)], neutral_edges=[(0,1,1)])

        if not with_farey_density and not with_gauss_density:
            return S

        output = [S]

        if with_farey_density:
            x = polygen(QQ, 'x')
            output.append([1/(1+x), 1/(x-2)/(x-3), 1/x/(1+x), (x**2-4*x+5)/(1-x)/(2-x)/(3-x)])

        if with_gauss_density:
            x = polygen(QQ, 'x')
            output.append([[(x + 1)**-1 * (x + 2)**-1, (x + 2)**-1],
                           [(QQ(1)/2) * (x + QQ(3)/2)**-1 * (x + 2)**-1,
                            (x + 1)**-1 * (x + QQ(3)/2)**-1 * (x + 2)**-1 * (x**2 + 3*x + QQ(5)/2)]])

        return output

    @staticmethod
    def irrational(with_farey_density=False, with_gauss_density=False):
        S = SimplicialSytemGenerator.up_down(2)
        S = S.flip_flop_extension(plus_edges=[(0,0,0)], minus_edges=[(1,1,1)], neutral_edges=[(1,0,0),(0,1,1)])
        if not with_farey_density and not with_gauss_density:
            return S

        output = [S]

        if with_farey_density:
            x = polygen(QQ, 'x')

        if with_gauss_density:
            x = polygen(QQ, 'x')
            output.append([[],[]])
            raise NotImplementedError

        return output

    @staticmethod
    def mercat_irrational(with_farey_density=False, with_gauss_density=False):
        G = DiGraph([(0,0,0),(0,3,1),(3,3,1),(3,1,0),(1,0,0),(1,2,1),(2,1,0),(2,2,1)], loops=True)
        S = simplicial_system_dim2.SimplicialSystemDim2(G)
        if not with_farey_density and not with_gauss_density:
            return S

        f0 = 3/(h+3)
        f1 = 3/(h+2)/(h+3)
        f2 = 5.941768373667762 / (h^2 + 4.857371474615395*h + 5.941768373667762)
        f3 = (1.782035847014518*h + 3.8769005196942716)/(0.9999999999999999*h^2 + 3.9194687090178286*h + 3.8769005196942716)

        output = [S]
        return output

    @staticmethod
    def two_vertex(with_farey_density=False, with_gauss_density=False):
        G = DiGraph([(0, 1, 1), (0, 1, 0), (1, 0, 1), (1, 1, 0)], loops=True, multiedges=True)
        S = simplicial_system_dim2.SimplicialSystemDim2(G)
        return S
        x = polygen(QQ, 'x')
        f0 = 2/(x+2)

    @staticmethod
    def SternBrocotTree(rat):
        from sage.rings.infinity import Infinity
        if not isinstance(rat, Rational) or not 0 <= rat <= 1:
            raise ValueError

        T = DiGraph(loops=True)
        intervals = [((-QQ(1), QQ(1)), (QQ(0), QQ(1))), ((QQ(0), QQ(1)), (QQ(1), QQ(1)))]
        while not any(rat == p/q or rat == r/s for (p, q), (r, s) in intervals):
            new_intervals = []
            for (p, q), (r, s) in intervals:
                m, n = p + r, q + s
                T.add_edge(((p/q, r/s), (p/q, m/n), 0))
                T.add_edge(((p/q*I, r/s*I), (p/q*I, m/n*I), 1))
                new_intervals.append(((p, q), (m, n)))
                T.add_edge(((p/q, r/s), (m/n, r/s), 1))
                T.add_edge(((p/q*I, r/s*I), (m/n*I, r/s*I), 0))
                new_intervals.append(((m, n), (r, s)))
            intervals = new_intervals
        T.add_edge(((0, Infinity), (0, Infinity), 1))
        T.add_edge(((0, Infinity), (0, 1), 0))
        T.add_edge(((-Infinity, 0), (-Infinity, 0), 0))
        T.add_edge(((-Infinity, 0), (-1, 0), 1))
        T.add_edge(((0, I*Infinity), (0, I*Infinity), 0))
        T.add_edge(((0, I*Infinity), (0, I), 1))
        T.add_edge(((-I*Infinity, 0), (-I*Infinity, 0), 1))
        T.add_edge(((-I*Infinity, 0), (-I, 0), 0))

        for (p, q), (r, s) in intervals:
            if p/q >= rat:
                T.add_edge(((p/q, r/s), (p/q - 1, r/s - 1), 2))
                T.add_edge(((p/q*I, r/s*I), ((p/q - 1)*I, (r/s - 1)*I), 2))
            elif r/s <= rat - 1:
                T.add_edge(((p/q, r/s), (p/q + 1, r/s + 1), 2))
                T.add_edge(((p/q*I, r/s*I), ((p/q + 1)*I, (r/s + 1)*I), 2))
            else:
                if p >= 0 :
                    x = s/r - floor(s/r)
                    y = +Infinity if p == 0 else q/p - floor(s/r)
                    T.add_edge(((p/q, r/s), (x*I, y*I), 3))
                    T.add_edge(((p/q*I, r/s*I), (x, y), 3))
                if r <= 0:
                    x = -Infinity if r == 0 else s/r - ceil(q/p)
                    y = q/p - ceil(q/p)
                    T.add_edge(((p/q, r/s), (x*I, y*I), 3))
                    T.add_edge(((p/q*I, r/s*I), (x, y), 3))

        return SimplicialSystem(T).reduce()

simplicial_systems = SimplicialSystemGenerators()
