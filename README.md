# Multidimensional Continued Fractions

This project implements a python object for simplicial systems.

Simplicial systems provide a useful representation of simplex-splitting linear continued fraction algorithms as well as some important fractals in number theory.
In several jupyter worksheet we use it to study some examples, namely :

	* Brun algorithm
	* Selmer algorithm
	* Jacobi--Perron algorithm
	* Rauzy induction
	* Rauzy gasket


# Reference

	- [AL18] Arnoux, Labbé.  On some symmetric multidimensional continued fraction algorithms.  Ergodic Theory Dynam. Systems, 38(5):1601–1626, 2018.
	- [Fou20] Fougeron. Dynamical properties of simplicial systems and continued fraction algorithms. arXiv:2001.01367, 2020.
