def cycles(f):
    r"""
    Return the cycles of the function ``f`` (in other words the cycle decomposition
    of the part of the domain on which f is bijective).

    INPUT:

    - ``f`` - a list encoding a function on `{0, 1, ..., n-1}`

    EXAMPLES::

        sage: cycles([1,0,0])
        [[0, 1]]
        sage: cycles([1,0,0,2,4])
        [[0, 1], [4]]
        sage: cycles([1,2,2])
        [[2]]
    """
    cycles = []
    n = len(f)
    seen = [0] * n
    for u in range(n):
        if seen[u]:
            continue
        v = u
        while seen[v] == 0:
            seen[v] = 1
            v = f[v]
        if seen[v] == 1:
            # found a cycle from v
            c = [v]
            v = f[v]
            while v != c[0]:
                c.append(v)
                v = f[v]
            cycles.append(c)

        # mark seen vertices as 2
        while seen[u] != 2:
            seen[u] = 2
            u = f[u]

    return cycles

def matrix_all_distances(f):
    r"""
    Return the cycles of the function ``f`` (in other words the cycle decomposition
    of the part of the domain on which f is bijective).

    INPUT:

    - ``f`` - a list encoding a function on `{0, 1, ..., n-1}`

    EXAMPLES::

        sage: matrix_all_distances([1,0,0])
        [[0, 1, -1],
         [1, 0, -1],
         [1, 2, 0]]
        sage: matrix_all_distances([1,0,0,2,4])
        [[0, 1, -1, -1, -1],
         [1, 0, -1, -1, -1],
         [1, 2, 0, -1, -1],
         [2, 3, 1, 0, -1],
         [-1, -1, -1, -1, 0]]
        sage: matrix_all_distances([1,2,2])
        [[0, 1, 2],
         [-1, 0, 1],
         [-1, -1, 0]]
    """
    n = len(f)
    distances = [[-1] * n for _ in range(n)]

    for u in range(n):
        # compute distances from u (ie row u of the matrix)
        v = u
        k = 0
        while distances[u][v] == -1:
            distances[u][v] = k
            v = f[v]
            k += 1
    return distances
